# 챕터 4. 나만의 사진 포트폴리오 만들기

## 자바스크립트 배우기
* [01. Javascript와의 첫 만남](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/01/README.md)
* [02. Javascript 자료형과 변수](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/02/README.md)
* [03. Javascript Boolean 자료형과 조건문](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/03/README.md)
* [04. Javascript Boolean 배열과 반복문](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/04/README.md)
* [05. Javascript 함수와 이벤트](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/05/README.md)
* [06. Javascript 객체와 스타일 다루기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/06/README.md)
* [07. Javascript 자료형 더 자세히 알아보기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/07/README.md)
* [08. Javascript 실습을 위한 DOM 기능들](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/08/README.md)

<br>

## 포트폴리오에 생명 불어넣기

<a href="https://fastcampus-all.netlify.com/2020-spring/04-javascript/final-practice" target="_blank">최종 완성본 보기</a>

* [01. 메뉴마다의 화면 보여주기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-01/README.md)
* [02. 사진 올리기 페이지](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-02/README.md)
* [03. 내 정보 수정 & 저장하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-03/README.md)
* [04. 썸네일 나타내고 좋아요 토글하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-04/README.md)
* [05. 정렬과 필터 적용하기](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-05/README.md)