# Practice 01. 메뉴마다의 화면 보여주기

<a href="https://fastcampus-all.netlify.com/2020-spring/04-javascript/practice-02" target="_blank">이번강 완성본 보기</a>
<br>
<a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/04-javascript/practice-01/practice-01.zip" target="_blank">
  [실습파일 다운로드 받기]
</a>
<br>
<br>

## 선택된 메뉴값에 따라 요소들의 모습 보여주기 

### 방법 1. 각 요소들의 클래스를 변경

> `nav` 요소 내의 버튼들
```html
<li class="gallery on">사진들 보기</li>
<li class="upload">사진 올리기</li>
<li class="myinfo">내 정보</li>
```
```css
header .nav li.on {
  border-bottom: 4px solid white;
  height: 60px;
}
```

<br>

### 방법 2. 최상위 부모 요소의 클래스를 변경

> `main` 요소
```html
  <main class="gallery">
```
```css
main .dep { display: none; }
main.gallery .dep._gallery,
main.upload .dep._upload,
main.myinfo .dep._myinfo {
  display: block;
}
```

<br>

***

<br>

## 함수와 버튼으로 제어하기
* 메뉴 클래스 값을 인자로 받는 함수를 둠
* 각 버튼에 클래스대로 인자를 넣어 함수에 연결

```javascript
function setMenu(_menu) {
  // ...
}
```
```html
<li class="gallery on" onclick="setMenu('gallery')">사진들 보기</li>
<li class="upload" onclick="setMenu('upload')">사진 올리기</li>
<li class="myinfo" onclick="setMenu('myinfo')">내 정보</li>
```

<br>

### `setMenu` 함수 내용부

> 모든 메뉴 버튼의 `on` 해제하기
```javascript
var filterButtons = document.querySelectorAll("nav li");
filterButtons.forEach(function (filterButton) {
  filterButton.classList.remove("on");
});
```

<br>

> 선택된 메뉴 버튼 `on`으로 설정하기
```javascript
document.querySelector("nav li." + _menu).classList.add("on");
```

<br>

> `main` 요소 클래스 설정
```javascript
document.querySelector("main").className = _menu;
```

<br>

***

<br>

## 다음 강좌
* [02. 사진 올리기 페이지](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/04-javascript/practice-02/README.md)