# 웹 개발자 로드맵

## 🗺 그림으로 훑어보기
* <a href="https://github.com/kamranahmedse/developer-roadmap" target="_blank">이미지 로드맵 링크</a>  
* <a href="https://github.com/devJang/developer-roadmap" target="_blank">한글판 보기</a>

<br>

## **Front-end** vs. **Back-end**

![front vs back](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/01/images/05.png)   


## 👩 Web Publisher
> 👏 여러분은 현재 이 단계에 있습니다.

![html,css,javascrpt](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/01/images/01.png)   

* `HTML`과 `CSS`의 주요 태그들과 속성, 기능들에 대한 지식과 활용 능력
  - 디자이너가 제작한 시안대로 화면을 만들어낼 수 있는 실력
* 웹 접근성과 크로스 브라우징에 대한 이해
* `JavaScript`에 대한 이해
  - DOM을 다룰 수 있는 실무능력

<br>
<br>

# 새로 배워나가야 할 것들

## 📚 git & Github
![git, github](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/01/images/02.png)   

### <a href="https://git-scm.com/" target="_blank">Git - 버전 관리 시스템</a>
* 코드 등 프로젝트에 사용되는 파일들의 버전을 관리
*   > *`시간여행`* & *`평행우주`*
* **<a href="https://www.sourcetreeapp.com/" target="_blank">SourceTree</a>** 등 GUI 소프트웨어로도 가능

![git](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/01/images/03.png)   

<br>

### <a href="https://github.com/" target="_blank">Github - 온라인 Git 저장소 서비스</a>
* Git으로 관리되는 파일, 내역들을 업로드하는 공간
* > *`오픈소스의 성지`*

<br>

***

<br>

## 📚 <a href="https://developer.mozilla.org/ko/docs/Learn/CSS/CSS_layout/%EB%B0%98%EC%9D%91%ED%98%95_%EB%94%94%EC%9E%90%EC%9D%B8" target="_blank">반응형 디자인</a>

![responsive design](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/01/images/06.png)   

<br>


### <a href="https://developer.mozilla.org/ko/docs/Web/Guide/CSS/Media_queries" target="_blank">미디어 쿼리</a>
```css
@media screen and (max-width: 799px) { 
  .container { 
    width: 100%;
  } 
} 
@media screen and (min-width: 800px) { 
  .container { 
    width: 800px;
  } 
} 
```


<br>

***

<br>

## 📚 <a href="https://jquery.com/" target="_blank">jQuery</a>
⛔️ 쇠퇴하는 방식 - 아직 사용하는 회사들이 있지만 *`미리 익혀둘 필요 없음`*

* 2000년대 말 ~ 2010년대를 풍미했던 자바스크립트 **`라이브러리`**
* *`write less, do more`*
* 브라우저 호환

```javascript
// 순수 자바스크립트

document.querySelector("#popup").style.display = 'block';
document.querySelector("#popup").style.display = 'none';

document.querySelector("#button").addEventListener('click', function () {
  console.log("Button Pressed");
});
```

```javascript
// jQuery

$("#popup").show();
$("#popup").hide();

$("#button").click(function () {
  console.log("Button Pressed");
});
```

<br>

🗑 오늘날은 환경이 달라짐 : 자바스크립트와 브라우저들의 발전
> 순수 자바스크립트(`Vanilla JS`)를 꾸준히 공부하고  
> jQuery는 회사가 사용할 시 배우는 것으로 충분

<br>

***

<br>

## 📚 <a href="https://getbootstrap.com/" target="_blank">Bootstrap</a>
![jquery](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/raw/master/2020-spring/06-roadmap/01/images/04.png)   

웹 디자인 **`프레임워크`**  
> 디자이너의 도움이나 없이 또는 많은 디자인 작업 없이 예쁜 사이트를 만들고 싶다면 유용

* `css`와 `js` 파일들을 가져와 연결함으로써 정형화된 디자인 요소들 사용

<br>

***

<br>

## 다음 강좌
* [02. 프론트엔드 개발자](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/06-roadmap/02/README.md)